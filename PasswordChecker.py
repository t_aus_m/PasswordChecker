#!/usr/bin/env python

import csv
import hashlib
import requests
import time

reqUrl = 'https://api.pwnedpasswords.com/range/'

pwFile = "./export.csv"
pwdField = 'Password'
accName = 'Title'

hashTable = []
accLookup = []

with open(pwFile,"r") as csvfile:
	pwReader = csv.DictReader(csvfile)
	for row in pwReader:
		hashTable.append(hashlib.sha1(row[pwdField]).hexdigest())
		accLookup.append(row[accName])

print("The following entries have been found:")
i = 0
for entry in hashTable:
	ret = requests.get(reqUrl + entry[:5]).content.split("\r\n")		
	ret = [elem.split(":",1)[0].lower() for elem in ret]
	if (entry[5:] in ret):
		print(accLookup[i])
	i+=1
	time.sleep(.150)

