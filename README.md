# PasswordChecker

This small script compares the hashes of your KeePass-Database .csv-Export against the database of [https://haveibeenpwned.com](haveibeenpwned.com). 

### Please note:

The export has to be named `export.csv` or the name of the file has to be changed in the script.